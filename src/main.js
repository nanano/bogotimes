import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import StoryblokVue from 'storyblok-vue';
import VueMarkdown from 'vue-markdown';
import VueMeta from 'vue-meta';
//PAGES IMPORT

//import firebase
import firebase from 'firebase/app';
import 'firebase/analytics';

var firebaseConfig = {
  apiKey: process.env.VUE_APP_FB_APIKEY,
  authDomain: "bogotimes-94ece.firebaseapp.com",
  databaseURL: "https://bogotimes-94ece.firebaseio.com",
  projectId: "bogotimes-94ece",
  storageBucket: "bogotimes-94ece.appspot.com",
  messagingSenderId: "137959961767",
  appId: "1:137959961767:web:2464532cb8942a9fb91782",
  measurementId: "G-CS5DW56NKV"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

Vue.config.productionTip = false

Vue.use(StoryblokVue);
Vue.use(VueMarkdown);
Vue.use(VueMeta);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')

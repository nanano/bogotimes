import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
    icons: {
        iconfont: 'mdi',
    },
    theme: {
      options: {
        customProperties: true
      },
      themes: {
        light: {
          primary: '#333333',
          secondary: '#FFFFFF',
          accent: '#2AABF2',
          error: '#FF4444',
        },
      },
    },
    lang: {
        locales: 'es_co'
    },
});
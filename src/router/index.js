import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Articles from '../views/Articles.vue'

import HistoryArticles from '../views/History.vue'
import Events from '../views/Events.vue'
import ArticleDetail from '../views/details/ArticleDetail'
import HistoryDetail from '../views/details/HistoryDetail'
import AboutUs from '../views/About.vue'

//ARTICLES
import Opinion from '../views/Opinion.vue'
import Actualidad from '../views/Actualidad.vue'
import Notas from '../views/Notas.vue'
import Arte from '../views/Arte.vue'
import Cronicas from '../views/Cronicas.vue'
import Lectura from '../views/Lectura.vue'


Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/articulos',
    name: 'Articles',
    component: Articles
  },
  {
    path: '/articulos/:id',
    name: 'Article',
    component: ArticleDetail
  },
  {
    path: '/opinion',
    name: 'Opinion',
    component: Opinion
  },
  {
    path: '/opinion/:id',
    name: 'Opinion',
    component: ArticleDetail
  },
  {
    path: '/actualidad',
    name: 'Actualidad',
    component: Actualidad
  },
  {
    path: '/actualidad/:id',
    name: 'Actualidad',
    component: ArticleDetail
  },
  {
    path: '/notas',
    name: 'Notas',
    component: Notas
  },
  {
    path: '/notas/:id',
    name: 'notas',
    component: ArticleDetail
  },
  {
    path: '/arte',
    name: 'Arte',
    component: Arte
  },
  {
    path: '/arte/:id',
    name: 'Arte',
    component: ArticleDetail
  },
  {
    path: '/cronicas',
    name: 'Cónicas',
    component: Cronicas
  },
  {
    path: '/cronicas/:id',
    name: 'Cónicas',
    component: ArticleDetail
  },
  {
    path: '/lectura',
    name: 'Lectura',
    component: Lectura
  },
  {
    path: '/lectura/:id',
    name: 'Lectura',
    component: ArticleDetail
  },
  {
    path: '/cita-con-la-historia',
    name: 'History',
    component: HistoryArticles
  },
  {
    path: '/quienes-somos',
    name: 'Quienes somos',
    component: AboutUs
  },
  {
    path: '/cita-con-la-historia/:id',
    name: 'History',
    component: HistoryDetail
  },
  {
    path: '/eventos',
    name: 'Events',
    component: Events
  },
  {
    path: '/promociona',
    name: 'Promotion',
    component: Home
  },
  {
    path: '/terminos-y-condiciones',
    name: 'Terms',
    component: Home
  },
  {
    path: '/donar',
    name: 'Donation',
    component: Home
  }
]

const router = new VueRouter({
  routes,
  mode: 'history',
  scrollBehavior () {
    return { x: 0, y: 0 };
  }
})

export default router

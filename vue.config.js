module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  "pwa": {
    "manifestCrossorigin": "anonymous",
    "name": "Bogotimes",
    "themeColor": "#212121",
    "background_color": "#ffffff",
  }
}